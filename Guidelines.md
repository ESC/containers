# ESC Container Guidelines

**Edited by** D. Blyth, W. Deconinck, M. Diefenthaler, A. Dotti, A. Kiselev, D. Lawrence 
> The current version of this document will focus on an interactive VM style container. A future version will include guidelines for an appliance style container appropriate for use on GRID sites, commercial cloud systems, and HPC resources. This version will include guidelines for deriving Singularity and Shifter images from the Docker images.

## Introduction
This document provides guidelines for producing container images that provide EIC software. The primary goal of the images is to provide an easy means for scientists to start running EIC software. Specifically targeted are persons with limited time to invest in building the software and all of its prerequisites. The guidelines will help ensure that software developed at different sites (ANL, BNL, JLab) is packaged in a similar way simplifying cross comparisons and will evolve over time to reflect changes in the EIC software or in the requirements of the EIC community. 

## Goals
* Make it easier for users to work on the physics program and detector design for the EIC, e.g., by minimizing the "installation overhead" for new users. 
* Allow EIC users to run the EIC software interactively on any computer (personal desktop or laptop) through the use of containers; one should be able to run the same container image under any modern 64-bit Linux, Mac OS, or MS Windows system.
* Provide consistency between software generated at different facilities. 

## Technology 

* Docker generates initial image; all the content should go into Dockerfile and associated input files if needed. 
* Consideration should be given to repeatability of builds from the Dockerfile.
* Consideration should be given to using the image with Singularity or Shifter by simple import from Docker (not be worked out in detail for the first versions).
* Consideration should be given to how the image may eventually be used as an appliance (also not be worked out in detail for the first versions). 

## EIC computing environment 

Different groups within the ESC are developing software stacks with pieces that are independent from other groups. Some of the development environments are incompatible making it impractical at this time to impose a single OS/compiler. This document describes a set of software tools common to the field and directory structure to be used in the container. This common format should make it easier for EIC users to use any of the containers once they have familiarized themselves with one.

### Software

Images will contain at a minimum:
* **Development tools** cmake3, Geant4, git, gcc, make, python
* **HepSim support** Google protocol buffer

In addition, the following packages are recommended:
* **Analysis tools** CLHEP, ROOT6
* [Graphical environment](https://gitlab.com/ESC/containers/tree/master/Docker/JLEIC/jleicgx) 
    * Documentation about the graphics choices should be provided in **/eic/doc.** 
    * It is recommended that containers implement a solution that allows users to access the container’s graphics via the host system’s native web browser.
    * Use of other technologies such as X that require additional software on the host system should be clearly documented so end users know the requirements. These solutions should eventually be supplemented by a browser-based one (e.g. noVNC). 
> To be clear, this bullet is not meant to address X within the container. Only the case of it being used as the communication mechanism between software inside the container and outside of the container which would require an X server on the host system.

### System

* Images may be built on any Linux system deemed appropriate by the authors. It is noted that [CERN has changed OS from Scientific Linux to CentOS](http://linux.web.cern.ch/linux/centos.shtml) and most end users will have some familiarity with this OS. 

* Containers should implement a user with name *eicuser* with bash as the shell. 

* The eicuser should be given no-password sudo privilege within the container. 

* Environment variables should be set explicitly via Dockerfile instructions wherever practical:
    * This is to allow easier conversion of the VM style image to an Appliance style later
    * When a more complicated setup is required, it is strongly encouraged to be done in **/etc/profile.d**.
    * 
All other software is installed in **/container** with links pointing to it from** /eic**: 
* **/eic/app** is the directory for software commonly used in NP, e.g., Geant4 and ROOT, and the EIC Software, e.g., EicRoot or GEMC. We might have an **/eic/lib** directory as soon as we have EIC libraries we would like to share. 
* **/eic/doc** is the directory for the user documentation. There are subdirectories for information about the EIC implementation proposals, eRHIC and JLEIC, and the software described, e.g., EicRoot and GEMC. Each of the software documentation should contain examples that work with the software in the container. A suggested format for documentation is Markdown, or any format that is easily readable in a terminal.

The binaries are actually installed in the **/container** tree. The directory tree of **/eic** has symbolic links pointing back to the **/container** tree as well as *PRO* links pointing to the specific version in the **/eic** tree. This should allow one to modify the links in the **/eic** directory to point to an externally mounted directory (e.g., CVMFS) while still leaving the container-resident software accessible via **/container**. Container image developers should avoid hardcoding library dependencies via *RPATH* wherever possible. An example of the initial structure for non-OS-installed software is shown below: 

```
  /container
    `-- app
        |-- clhep
        |   `-- CLHEP_2_3_4_5
        |-- geant4
        |   `-- 10.3.3
        |-- qt
        |   `-- 5.6.2
        `-- root
             `-- 6.10.08
```

```
  /eic
   `-- app
    |-- clhep
    |   |-- CLHEP_2_3_4_5 -> /container/app/clhep/CLHEP_2_3_4_5
    |   `-- PRO -> CLHEP_2_3_4_5
    |-- geant4
    |   |-- 10.3.3 -> /container/app/geant4/10.3.3
    |   `-- PRO -> 10.3.3
    |-- qt
    |   |-- 5.6.2 -> /container/app/qt/5.6.2//gcc_64
    |   `-- PRO -> 5.6.2
    `-- root
        |-- 6.10.08 -> /container/app/root/6.10.08
```

## Features

* Welcome message (**/etc/motd**): This should give some basic information for a new user on getting started, e.g., *EIC image jleic version 1.2.3 for help getting started, type:  less /etc/doc/GettingStarted.md*
* The image contains a copy of the Dockerfile and supporting files used to create the image in a standard location: **/image.** This provides a means of reproducing an image with nothing more than the image itself. Since images may be built in stages using different Dockerfiles, each should create its own subdirectory in /**image**. Each Dockerfile should include comments at the top indicating how to build and deploy the image. An example is shnown below: 
```
/image
├── eic
│   ├── Dockerfile
│   ├── eic_xstart.csh
│   ├── eic_xstart.sh
│   ├── README.md
│   ├── xstart.csh
│   ├── xstart.sh
│   └── xstop
├── eic-base
│   └── Dockerfile
└── eic-deps
    └── Dockerfile
```

## Deployment

* Images will be deployed using  the electronioncollider swarm on Docker Hub. 
* In addition, any Dockerfiles should be committed to the ESC/containers repository on GitLab. Any required supporting files should be included or clear instructions given on where to obtain them. 

