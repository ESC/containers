This the repository In the repository, the ESC maintains information for producing container images that contain EIC software. The primary goal of the images is to provide an easy means for scientists to start running EIC software.

The ESC Container project is coordinated by [David Lawrence](mailto:davidl@jlab.org). 
