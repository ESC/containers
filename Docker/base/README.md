# About
Provide common base layers for ESC software efforts.

Compiled images are currently hosted with the tags
* electronioncollider/centos
* electronioncollider/eic-base
* electronioncollider/eic
* electronioncollider/eicgx

# Contents

### centos

depends on:  
```docker
FROM centos:7.<minor>.<build>
```

The `centos` image provides...
* CentOS 7 base system with
    * corrected permissions on some common directories
    * exported and flattened

### eic-deps

depends on:  
```docker
FROM electronioncollider/centos:
```

the `eic-deps` image provides ROOT, CLHEP, GEANT and their prerequesties


### eic-base

depends on:  
```docker
FROM electronioncollider/eic-deps
```

The `eic-base` image provides...

* CentOS 7 base system with
    * gcc-c++, gcc-fortran
    * make, cmake3, scons
	* python
    * git
	* X11, Xt, Xmu, Xpm, Xft , mesa-libGLU, qt5, xerces-c
	* plus a few other packages ...
* CLHEP
* GEANT4
* ROOT6

(the image adds GEANT4 data files)

### eic

depends on:  
```docker
FROM electronioncollider/eic-base
```

noVNC-webserver and VNC-server that can be used to view
graphics on the host using either a native webrowser (HTML5 enabled)


# Other features
* A default user named `eicuser` is provided in the image.  This user has sudo
  access. 
* The image is designed to have the appropriate environment variables without
  any entrypoint scripts.  The default command is `bash`.

# Software versions
Different software versions in eic-base may be built without modifying the Dockerfile. 
See the comments at the top of the file.  The intended use is to build required
software versions and tag them appropriately in the image repository.  Be aware
that the `eic` and `eicgx` images may not provide the appropriate GEANT4 data files,
depending on the GEANT4 version.
