#!/bin/bash

if [ $# -eq 0 ] ; then
	echo "Usage: $0 [tag=7.4.1708] [repo=electronioncollider]"
	exit
fi

tag=${1:-7.4.1708}
repo=${2:-electronioncollider}

sudo docker build -t centos:${tag} .
sudo docker run --name forexport -d centos:${tag}
sudo docker export forexport > centos_${tag}.tar
sudo docker rm forexport
sudo docker rmi centos:${tag}
sudo docker import centos_${tag}.tar ${repo}/centos:${tag}
rm centos_${tag}.tar
sudo docker push ${repo}/centos:${tag}
sudo docker rmi ${repo}/centos:${tag}
