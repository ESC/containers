#!/bin/sh

# Script based on set_eic from subversion repository
#
# This script was placed here when the docker image was created.

# The following are modified by the Dockerfile during image creation
export JLAB_ROOT=__JLAB_ROOT__
export JLAB_VERSION=__JLAB_VERSION__
export JLEIC_ROOT=__JLEIC_ROOT__


source $JLAB_ROOT/$JLAB_VERSION/ce/jlab.sh
export LD_LIBRARY_PATH=${GEMC}:${LD_LIBRARY_PATH} 
export PATH=${EIC_GEMC}/source/${GEMC_VERSION}:${PATH} 

export EIC_GEMC=$JLEIC_ROOT/eic_svn/eic_gemc #eic_gemc location

export GEMC_HOST=eicdb.jlab.org   #use this if your system is on jlab network
#export GEMC_HOST=127.0.0.1       #use this if you are tunnleing to access mysql server
export GEMC_DB=eic_geometry       #the official detector database
export GEMC_BANK=eic_banks        #the official output bank info
#export GEMC_DB=test              #user geometry for test
#export GEMC_BANK=test            #user banks for test
export GEMC_USER=eicuser          #this user only can read database, used for run simulation
export GEMC_PASS=iloveeic         #readonly user password
#export GEMC_USER eicdev          #this user can read and write, used for update database
#export GEMC_PASS                 #ask other solid gemc develeoper for this password
