
# JLEIC (Jefferson Lab EIC) container

## Introduction

This directory contains files for building a Docker image that contains
software for running eic simulations.

A pre-built image is available on docker cloud so you don't need to build
your own. See instructions below.


## Using pre-built image

Pre-built images of some versions are available on docker cloud. Here's
how to use one on a computer where docker is already installed.

> docker run -it --rm -e DISPLAY=`hostname`:0.0 jeffersonlab/jleic

The "-e DISPLAY=`hostname`:0.0" option is used to point to the X-windows
server on the host. It allows for graphics, but can be omitted if only
running batch mode.
