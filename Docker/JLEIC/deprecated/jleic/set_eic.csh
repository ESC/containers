#!/bin/tcsh -f

# Script based on set_eic from subversion repository
#
# This script was placed here when the docker image was created.

# The following are modified by the Dockerfile during image creation
setenv JLAB_ROOT __JLAB_ROOT__
setenv JLAB_VERSION __JLAB_VERSION__
setenv JLEIC_ROOT __JLEIC_ROOT__


source $JLAB_ROOT/$JLAB_VERSION/ce/jlab.csh
setenv LD_LIBRARY_PATH ${GEMC}:${LD_LIBRARY_PATH} 
setenv PATH ${EIC_GEMC}/source/${GEMC_VERSION}:${PATH} 

setenv EIC_GEMC $JLEIC_ROOT/eic_svn/eic_gemc #eic_gemc location

setenv GEMC_HOST eicdb.jlab.org   #use this if your system is on jlab network
#setenv GEMC_HOST 127.0.0.1       #use this if you are tunnleing to access mysql server
setenv GEMC_DB eic_geometry       #the official detector database
setenv GEMC_BANK eic_banks        #the official output bank info
#setenv GEMC_DB   test              #user geometry for test
#setenv GEMC_BANK test              #user banks for test
setenv GEMC_USER eicuser          #this user only can read database, used for run simulation
setenv GEMC_PASS iloveeic         #readonly user password
#setenv GEMC_USER eicdev          #this user can read and write, used for update database
#setenv GEMC_PASS                   #ask other solid gemc develeoper for this password
