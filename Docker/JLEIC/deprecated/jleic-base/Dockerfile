#--------------------------------------------------------------------------
# jleic-base
# 
# This Dockerfile describes the base container image for JLEIC
# Software. It contains CLHEP, Geant4, and ROOT6 along with a number of
# system installed packages and the eicuser account.
#
# In order to build the image with a different version of, e.g., GEANT4, use the
# `--build-arg` flag, e.g.
# ```shell
# docker build --build-arg GEANT4_VERSION=10.4.0.beta .
# ```
#
# The structure of the final image will have software installed in directories
# with a format:
#
#    /container/app/packageX/version
#
#  and symlinks
#
#    /eic/app/packageX/version -> /container/app/packageX/version
#    /eic/app/packageX/PRO     -> version
#
# The PATH and LD_LIBRARY_PATH envars will point to the PRO directories
# in the /eic tree. This structure is intended to allow the /eic directory
# to be replced with an externally mounted directory if desired.
# 
# Note that a default user `eicuser` is set up with sudo permissions.
# Additionally, the default command is `/bin/bash`.  A general
# convention is established here of explicitly stating environment variables as
# layers to avoid reliance on setup scripts where possible.
#
#
#--------------------------------------------------------------------------
#
# Build time: 
# Image size: 
#
#   docker build -t jleic-base:1.0.0 .
#
#--------------------------------------------------------------------------

FROM centos:7.4.1708

ARG CLHEP_TAG=CLHEP_2_3_4_5
ARG GEANT4_VERSION=10.3.3
ARG ROOT_VERSION=6.10.08

ARG NBUILD_THREADS=8

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Set up basic environment
RUN sed -i.bak 's/enabled=1/enabled=0/' /etc/yum/pluginconf.d/fastestmirror.conf

RUN yum install -y epel-release && \
    yum install -y \
		cmake3 \
		file \
        gcc-c++ \
		gcc-gfortran \
        git \
        make \
		python \
		scons \
        sudo \
		tcsh \
		wget \
		which \
	&& ln -s /usr/bin/cmake3 /usr/local/bin/cmake \
	&& rm -rf /var/cache/yum

RUN useradd -m -G wheel eicuser \
	&&  sed -i.bak 's/#\s*\(%wheel\s*ALL=(ALL)\s*NOPASSWD:\s*ALL\)/\1/' /etc/sudoers


# Place all EIC software, including support packages we compile ourselves
# in the /eic directory tree.
ENV EIC_ROOT /eic
ENV CONTAINER_ROOT /container
RUN mkdir -p $EIC_ROOT/app && chmod -R 777 $EIC_ROOT
RUN mkdir -p $CONTAINER_ROOT/app && mkdir -p $CONTAINER_ROOT/build && chmod -R 777 $CONTAINER_ROOT

ENV PATH ${PATH}:$EIC_ROOT/bin
ENV LD_LIBRARY_PATH $EIC_ROOT/lib

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Build and install system-wide CLHEP

ENV INSTALL_DIR_CLHEP $CONTAINER_ROOT/app/clhep/$CLHEP_TAG

RUN mkdir -p $CONTAINER_ROOT/build \
	&&  cd $CONTAINER_ROOT/build \
	&&  git clone https://gitlab.cern.ch/CLHEP/CLHEP.git \
	&&  cd CLHEP \
	&&  git checkout tags/$CLHEP_TAG \
	&&  mkdir build \
	&&  cd build \
	&&  mkdir -p $INSTALL_DIR_CLHEP \
	&&  CXXFLAGS=-std=c++11 cmake -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR_CLHEP ../ \
	&&  make -j $NBUILD_THREADS \
	&&  make install \
	&&  mkdir -p $EIC_ROOT/app/clhep \
	&&  ln -s $INSTALL_DIR_CLHEP $EIC_ROOT/app/clhep/$CLHEP_TAG \
	&&  ln -s $CLHEP_TAG $EIC_ROOT/app/clhep/PRO \
	&&  rm -rf $CONTAINER_ROOT/build

ENV PATH ${PATH}:${EIC_ROOT}/app/clhep/PRO/bin
ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${EIC_ROOT}/app/clhep/PRO/lib

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Build and install GEANT4

ENV INSTALL_DIR_GEANT4 $CONTAINER_ROOT/app/geant4/$GEANT4_VERSION

RUN yum install -y \
	assimp-devel \
	expat-devel \
	libX11-devel \
	libXt-devel \
	libXmu-devel \
	libXrender-devel \
	libXpm-devel \
	libXft-devel \
	libAfterImage \
	libAfterImage-devel \
	mesa-libGLU-devel \
	qt5-qtdeclarative-devel \
	qt5-linguist \
	tetgen-devel \
	xerces-c-devel \
	xkeyboard-config \
&&  rm -rf /var/cache/yum


# QT is installed above, but in a strange directory structure
# Make links here to provide a more traditional bin, lib, include structure.
# Then, use it when building GEANT4.
RUN mkdir -p $CONTAINER_ROOT/build \
	&&  echo '------------------- Setting up QT5 -----------------------' \
	&&  export QT_VERSION=`/usr/lib64/qt5/bin/qmake --version | grep Qt | awk '{print $4}'` \
	&&  export QTSYSTEM=gcc_64 \
	&&  export JLAB_SOFTWARE=$JLAB_ROOT/$JLAB_VERSION/$OSRELEASE \
	&&  export QTDIR=$CONTAINER_ROOT/app/qt/${QT_VERSION}/${QT_BIN_VERSION}/${QTSYSTEM} \
	&&  mkdir -p $QTDIR \
	&&  ln -s /usr/lib64 $QTDIR/lib \
	&&  ln -s /usr/lib64/qt5/bin $QTDIR/bin \
	&&  ln -s /usr/include/qt5 $QTDIR/include \
	&&  mkdir -p $EIC_ROOT/app/qt \
	&&  ln -s $QTDIR $EIC_ROOT/app/qt/${QT_VERSION} \
	&&  ln -s $QT_VERSION $EIC_ROOT/app/qt/PRO \
	&&  echo '--------------- Finished setting up QT5 ------------------' \
	&&  cd $CONTAINER_ROOT/build \
	&&  git clone https://github.com/Geant4/geant4.git \
	&&  cd geant4 \
	&&  git checkout tags/v$GEANT4_VERSION \
	&&  mkdir build \
	&&  cd build \
	&&  cmake \
        -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR_GEANT4 \
        -DGEANT4_BUILD_CXXSTD=11 \
        -DGEANT4_USE_GDML=ON \
        -DGEANT4_USE_SYSTEM_CLHEP=ON \
		-DCLHEP_ROOT_DIR=$INSTALL_DIR_CLHEP \
		-DGEANT4_USE_OPENGL_X11=ON \
		-DGEANT4_USE_RAYTRACER_X11=ON \
		-DGEANT4_BUILD_MULTITHREADED=ON \
		-DGEANT4_USE_QT=ON \
		-DCMAKE_PREFIX_PATH=$QTDIR \
        ../ \
	&&  make -j $NBUILD_THREADS \
	&&  make install \
	&&  mkdir -p $EIC_ROOT/app/geant4 \
	&&  ln -s $INSTALL_DIR_GEANT4 $EIC_ROOT/app/geant4/$GEANT4_VERSION \
	&&  ln -s $GEANT4_VERSION $EIC_ROOT/app/geant4/PRO \
	&&  rm -rf $CONTAINER_ROOT/build


ENV PATH ${PATH}:${EIC_ROOT}/app/geant4/PRO/bin
ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${EIC_ROOT}/app/geant4/PRO/lib

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Download and install Geant4 data files

ENV GEANT4_DATA_DIRECTORY ${INSTALL_DIR_GEANT4}/share/Geant4-10.3.3/data

RUN sudo mkdir $GEANT4_DATA_DIRECTORY

RUN curl -s http://geant4.cern.ch/support/source/G4NDL.4.5.tar.gz | sudo tar -xzf - -C $GEANT4_DATA_DIRECTORY
ENV G4NEUTRONHPDATA=${GEANT4_DATA_DIRECTORY}/G4NDL4.5

RUN curl -s http://geant4.cern.ch/support/source/G4EMLOW.6.50.tar.gz | sudo tar -xzf - -C $GEANT4_DATA_DIRECTORY
ENV G4LEDATA=${GEANT4_DATA_DIRECTORY}/G4EMLOW6.50

RUN curl -s http://geant4.cern.ch/support/source/G4PhotonEvaporation.4.3.2.tar.gz | sudo tar -xzf - -C $GEANT4_DATA_DIRECTORY
ENV G4LEVELGAMMADATA=${GEANT4_DATA_DIRECTORY}/PhotonEvaporation4.3.2

RUN curl -s http://geant4.cern.ch/support/source/G4RadioactiveDecay.5.1.1.tar.gz | sudo tar -xzf - -C $GEANT4_DATA_DIRECTORY
ENV G4RADIOACTIVEDATA=${GEANT4_DATA_DIRECTORY}/RadioactiveDecay5.1.1

RUN curl -s http://geant4.cern.ch/support/source/G4NEUTRONXS.1.4.tar.gz | sudo tar -xzf - -C $GEANT4_DATA_DIRECTORY
ENV G4NEUTRONXSDATA=${GEANT4_DATA_DIRECTORY}/G4NEUTRONXS1.4

RUN curl -s http://geant4.cern.ch/support/source/G4PII.1.3.tar.gz | sudo tar -xzf - -C $GEANT4_DATA_DIRECTORY
ENV G4PIIDATA=${GEANT4_DATA_DIRECTORY}/G4PII1.3

RUN curl -s http://geant4.cern.ch/support/source/RealSurface.1.0.tar.gz | sudo tar -xzf - -C $GEANT4_DATA_DIRECTORY
ENV G4REALSURFACEDATA=${GEANT4_DATA_DIRECTORY}/RealSurface1.0

RUN curl -s http://geant4.cern.ch/support/source/G4SAIDDATA.1.1.tar.gz | sudo tar -xzf - -C $GEANT4_DATA_DIRECTORY
ENV G4SAIDXSDATA=${GEANT4_DATA_DIRECTORY}/G4SAIDDATA1.1

RUN curl -s http://geant4.cern.ch/support/source/G4ABLA.3.0.tar.gz | sudo tar -xzf - -C $GEANT4_DATA_DIRECTORY
ENV G4ABLADATA=${GEANT4_DATA_DIRECTORY}/G4ABLA3.0

RUN curl -s http://geant4.cern.ch/support/source/G4ENSDFSTATE.2.1.tar.gz | sudo tar -xzf - -C $GEANT4_DATA_DIRECTORY
ENV G4ENSDFSTATEDATA=${GEANT4_DATA_DIRECTORY}/G4ENSDFSTATE2.1

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Build and install ROOT

ENV INSTALL_DIR_ROOT $CONTAINER_ROOT/app/root/$ROOT_VERSION

RUN mkdir -p $CONTAINER_ROOT/build \
	&&  cd $CONTAINER_ROOT/build \
	&&  wget https://root.cern.ch/download/root_v${ROOT_VERSION}.source.tar.gz \
	&&  tar xzf root_v${ROOT_VERSION}.source.tar.gz \
	&&  mkdir root-build \
	&&  cd root-build \
	&&  cmake \
        -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR_ROOT \
		-Dgdml=ON \
		-Dminuit2=ON \
		../root-${ROOT_VERSION} \
	&&  make -j $NBUILD_THREADS \
	&&  make install \
	&&  mkdir -p $EIC_ROOT/app/root \
	&&  ln -s $INSTALL_DIR_ROOT $EIC_ROOT/app/root/$ROOT_VERSION \
	&&  ln -s $ROOT_VERSION $EIC_ROOT/app/root/PRO \
	&&  rm -rf $CONTAINER_ROOT/build
	
	
ENV PATH ${PATH}:${EIC_ROOT}/app/root/PRO/bin
ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${EIC_ROOT}/app/root/PRO/lib

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Tidy up

RUN rm -rf $CONTAINER_ROOT/build

USER eicuser
WORKDIR /home/eicuser
CMD /bin/bash
