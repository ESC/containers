
# ESC OpenGL Graphics

## Introduction

This directory contains files for building a Docker image that
contains software capable of running OpenGL programs. It includes
a webserver that allows a browser to present an interactive desktop
allowing one to run multiple graphics programs in the container, and
even go full screen.


## Using pre-built image

A pre-built image of this is available on docker cloud. Here's
how to use one on a computer where docker is already installed.

```
> docker run -d -p 6080:6080 electronioncollider/escgx
```

Then, just point your browser to <http://localhost:6080>
(The pasword is "123456"). Try typing "glxgears" in the xterm to
check that OpenGL is working.

## Advanced Usage

The image includes a few scripts to make it a little easier to use
the container like a VM. These are run using the "xstart" alias
(defined for both bash and tcsh). The alias is used so that the
DISPLAY environment variable in the calling process can be set to
":0" allowing any graphics programs started there to go to the right
place. The script that the xstart alias runs (`eic_xstart.[c]sh`)
is executed as the default when the container is run with no arguments.
Thus, if you mount a local volume to work in while in the container,
you can actually use it as is for a development platform.

There are actually 3 ways to use graphics in the container:

1. Via web browser (HTML5 capable)
2. Via VNC client
3. Via X-windows

The easiest is to use X-windows by simply setting the DISPLAY environment
variable inside the container to point to the host. That relies on
the host running an X-server though and in many cases it will not 
work with OpenGL programs. To use X-windows directly like this does
not require anything special and can be done from any container that
has a graphical program installed.

The next easiest thing is to run a VNC server in the container and
expose the port to the host (e.g. by passing -p 5900:5900 to the 
docker run command). This requires a VNC client program on the host
which is generally pretty easy to find if it is not installed by
default. For example, you can use the built in vnc client on Mac OS X
with this container like this:

```
macosx:> docker run -d -p 6080:6080 jeffersonlab/jlabgx
macosx:> open vnc://localhost:5900
```

This works, but the graphics doesn't look so great and the performance
is poor relative to the web browser method.

The "hardest" (or maybe its the easiest) thing to do is to connect
a webserver in the container with the VNC server. This allows a
native web browser on the host to be used for the interface. This 
is the primary reason this container even exists since it is a little
more involved to get all the pieces working together. Using this method
seems to give better performance, probably due to the highly optimized
performance of modern web browsers. Again, using Mac OS X as an example:

```
macosx:> docker run -d -p 6080:6080 jeffersonlab/jlabgx
macosx:> open http://localhost:6080
```
You may find it easiest to use an xterm in the graphical window itself,
but you may also prefer to run programs from the native terminal program
where you launched the container. In this case, you can do something like
this:

```
> docker run -it --rm -p 6080:6080 jeffersonlab/jlabgx bash
# xstart
# glxgears
```

Where the last line should be replaced with whatever program you want
to run.


