# JLab Common Environment (CE) docker image
#
# The docker image includes both a noVNC-webserver
# and VNC-server that can be used to view graphics on the host using
# either a native webrowser (HTML5 enabled) or a native VNC viewer. This
# includes the ability to view OpenGL graphics.
#
# The graphics support was originally inspired by the Docker-opengl project
# here:
#
# https://github.com/thewtex/docker-opengl
#
# In the end, the only thing used from that was the idea to use
# noVNC. Thus, we do not include their license. However, I (David L.) do want to give
# them credit for inspiring this work.
#
# The container should be run with the "-p 6080:6080" option to allow
# outside connections to see the webserver. To connect to the VNC
# server directly, you should add the "-p 5900:5900" option. (Both
# can be included if you like.) Once inside the container, run "xstart"
# to fire up the servers. Then follow the directions for connecting.
#
# This Dockerfile is written by:
#
# David Lawrence
# Maurizio Ungaro
#
# Instructions:
#
#--------------------------------------------------------------------------
#
# Remember to find/replace the JLAB_VERSION with the newest one
#
# To build and push the container:
#
# docker build -f Dockerfile-2.3-mt -t jlabce:2.3-mt .
#
# docker tag jlabce:2.3-mt jeffersonlab/jlabce:2.3-mt
#
# docker push jeffersonlab/jlabce:2.3-mt
#
#--------------------------------------------------------------------------
#
# To run in batch mode:
#
#  docker run -it --rm jeffersonlab/jlabce:2.3-mt bash
#
# To use vnc or noVNC:
#
#   docker run -it --rm -p 6080:6080 -p 5901:5901 jeffersonlab/jlabce:2.3-mt
#
# On a mac, if you allow access from localhost with:
#
#  1. Activate the option ‘Allow connections from network clients’ in XQuartz settings (Restart XQuartz (to activate the setting)
#  2. xhost +127.0.0.1
#
# Then you can run docker and use the local X server with:
#
#  docker run-it --rm -e DISPLAY=docker.for.mac.localhost:0 jeffersonlab/jlabce:2.3-mt bash
#
#--------------------------------------------------------------------------

FROM centos:7.5.1804
LABEL maintainer "Maurizio Ungaro <ungaro@jlab.org>"

# Add enterprise linux repositories
RUN yum install -y epel-release

# Add graphics support
# mailcap needed for the novnc icons
RUN yum install -y \
	tigervnc-server \
	xfce4-session \
	xfce4-panel \
	xfce4-terminal \
	xfwm4 \
	mailcap \
	ghostscript-fonts \
	net-tools \
	glx-utils \
	python-websockify \
	xterm \
	nedit \
	git \
	&& yum clean all \
	&& rm -rf /var/cache/yum \
	&& git clone https://github.com/kanaka/noVNC.git /opt/noVNC \
	&& cd /opt/noVNC \
	&& ln -s vnc.html index.html \
	&& mkdir -p /container/utilities

# These define an alias and a script for easily starting up the
# servers from either bash or tcsh.
COPY xstart_alias.sh /etc/profile.d
COPY xstart_alias.csh /etc/profile.d
COPY xstart.sh /container/utilities
COPY xstart.csh /container/utilities
COPY xstop /usr/bin
ADD dot_config /root/.config
#
EXPOSE 6080 5900
#
#--------------------------------------------------------------------------
# JLab CE (Common Environment)
#
RUN yum install -y \
	cmake3 \
	make \
	scons \
	gcc-c++ \
	mysql \
	mysql-devel \
	python-devel \
	libX11-devel \
	mesa-libGLU-devel \
	libXt-devel \
	libXmu-devel \
	libXrender-devel \
	libXpm-devel \
	libXft-devel \
	expat-devel \
	bzip2 \
	qt5-qtbase-devel \
	qt5-linguist \
	wget \
	nano \
	which \
	tcsh \
	psmisc \
	&& yum clean all \
	&& ln -s /usr/bin/cmake3 /usr/local/bin/cmake

ENV JLAB_ROOT /jlab
ENV JLAB_VERSION 2.3
RUN mkdir -p $JLAB_ROOT/$JLAB_VERSION
WORKDIR $JLAB_ROOT
RUN rm -f ceInstall_$JLAB_VERSION.tar.gz \
	&& wget -c http://www.jlab.org/12gev_phys/packages/sources/ceInstall/ceInstall_$JLAB_VERSION.tar.gz \
	&& ln -s /usr/bin/qmake-qt5 /usr/bin/qmake \
	&& tar -zxpf ceInstall_$JLAB_VERSION.tar.gz --strip-components 1 -C $JLAB_VERSION \
	&& source $JLAB_ROOT/$JLAB_VERSION/ce/jlab.sh \
	&& cd $JLAB_ROOT/$JLAB_VERSION/install \
	&& ./go_clhep \
	&& ./go_xercesc \
	&& ./go_qt \
        && sed -i 's/MULTITHREADED=OFF/MULTITHREADED=ON/' ./go_geant4 \
	&& ./go_geant4 \
	&& ./go_sconsscript \
	&& ./go_evio \
	&& ./go_mysql \
	&& ./go_ccdb \
	&& ./go_mlibrary \
	&& ./go_gemc \
	&& ./go_root \
	&& ./go_banks\
	&& ./go_jana \
	&& rm -rf $JLAB_SOFTWARE/geant4/*/source \
	&& rm -rf $JLAB_SOFTWARE/root/root* \
	&& rm -rf $JLAB_SOFTWARE/xercesc/*/xerces-c-* \
	&& rm -rf $JLAB_SOFTWARE/mlibrary/*/cadmesh* \
	&& rm -rf $JLAB_SOFTWARE/jana/*/test \
	&& cd $JLAB_SOFTWARE/jana/*/plugins ; rm -f TestSpeed.so jana_iotest.so janactl.so ; cd - \
	&& ln -s $JLAB_ROOT/$JLAB_VERSION/ce/jlab.sh /etc/profile.d \
	&& ln -s $JLAB_ROOT/$JLAB_VERSION/ce/jlab.csh /etc/profile.d

ADD Dockerfile* /container/image/jlabce/
ADD xstart.csh /container/image/jlabce
ADD xstart.sh /container/image/jlabce
ADD xstart_alias.csh /container/image/jlabce
ADD xstart_alias.sh /container/image/jlabce
ADD xstop /container/image/jlabce
ADD README.md /container/image/jlabce
ADD CHANGES /container/image/jlabce
ADD dot_config /container/image/jlabce/dot_config

WORKDIR $JLAB_ROOT/$JLAB_VERSION/ce
CMD bash /container/utilities/xstart.sh && sleep infinity

