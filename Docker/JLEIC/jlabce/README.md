
# JLab Common Environment Container

### Notice: after jlabce 2.3 the dockerfiles used and maintained are moved to:

https://github.com/gemc/docker/tree/master/jlabce



## Introduction

This directory contains files for building a Docker image that contains
software setup by the JLab Common Environment (CE). This includes packages
like geant4, gemc, jana, xercesc, ... A full list and details on the 
CE can be found here:

<https://data.jlab.org/drupal/?q=node/55>

A pre-built image is available on docker hub so you don't need to build
your own. See instructions below.


## Using pre-built image

Pre-built images of some CE versions are available on docker cloud. Here's
how to use one on a computer where docker is already installed.

> docker run -it --rm jeffersonlab/jlabce:2.3



## Pre-requisites notes


JLAB CE:

- qt5-linguist  is needed for the command lupdate needed by scons
